require("lsp-inlayhints").setup({
  inlay_hints = {
    parameter_hints = {
      show = false,
    }
  }
})
vim.api.nvim_set_hl(0, "LspInlayHint", { link = "Comment" })

