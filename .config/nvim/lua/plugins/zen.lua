local cmp = require "cmp"

local plugins = {
  {
    "williamboman/mason.nvim",
    opts = {
      ensure_installed = {
        "rust-analyzer",
        "codelldb",
      },
    },
  },

  {
    'mrcjkb/rustaceanvim',
    version = '^4',
    lazy = false, -- This plugin is already lazy
    dependencies = {"neovim/nvim-lspconfig", "lvimuser/lsp-inlayhints.nvim"},
    ft = "rust",
    config = function()
      require "configs.rustaceanvim"
    end
  },

  {
    'rhaiscript/vim-rhai',
    ft = "rhai"
  },

  {
    'lvimuser/lsp-inlayhints.nvim',
    config = function()
      require "configs.inlayhints"
    end
  },

  {
    'saecki/crates.nvim',
    event = { "BufRead Cargo.toml" },
    config = function(_, opts)
      local crates  = require('crates')
      crates.setup(opts)
      require('cmp').setup.buffer({
        sources = { { name = "crates" }}
      })
      crates.show()
    end,
  },

  {
    "hrsh7th/nvim-cmp",
    opts = function()
      local M = require "nvchad.configs.cmp"
      M.completion.completeopt = "menu,menuone,noselect"
      M.mapping["<CR>"] = cmp.mapping.confirm {
        behavior = cmp.ConfirmBehavior.Insert,
        select = false,
      }
      table.insert(M.sources, {name = "crates"})
      return M
    end,
  },

  {
    "nvim-treesitter/nvim-treesitter",
    opts = {
      ensure_installed = {"html", "css", "dockerfile", "bash", "rust", "ron", "sql", "json", "xml", "lua", "javascript", "regex", "toml", "c", "cpp", "c_sharp", "cmake", "gitignore", "glsl", "wgsl", "wgsl_bevy"},
    },
  },
}
return plugins
