require "nvchad.mappings"

-- add yours here

local map = vim.keymap.set

map("n", ";", ":", { desc = "CMD enter command mode" })
-- map("i", "jk", "<ESC>")
map("v", "d", "\"_d", { noremap = true })

-- map({ "n", "i", "v" }, "<C-s>", "<cmd> w <cr>")

-- ZENNY
map({ 'n', 'v' }, '<PageDown>', '<c-d>zz', { noremap = true, silent = true })
map({ 'n', 'v' }, '<PageUp>', '<c-u>zz', { noremap = true, silent = true })
map("i", '<PageDown>', '<c-o><c-d><c-o>zz', { noremap = true, silent = true })
map("i", '<PageUp>', '<c-o><c-u><c-o>zz', { noremap = true, silent = true })
