alias gatus="git status"
alias gush="git push $@"
alias gommit="git commit -m $@"
alias gontinue="git rebase --continue"
alias gabort="git rebase --abort"
alias gadd="git add $@"
alias gupdate="git submodule update $@"
alias gamend="git commit --amend"
alias getch="git fetch $@"
alias geckout="git checkout $@"
alias granch="git branch $@"
alias giff="git diff $@"
alias gog="git log $@"
alias gull="git pull $@"
alias gupstream="git push --set-upstream origin $@"

alias src="source $@"

